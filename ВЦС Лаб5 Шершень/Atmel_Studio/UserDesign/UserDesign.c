// ######################################################################################
// #                                                                                    #
// #  This module implements the users design                                           #
// #                                                                                    #
// ######################################################################################

#include <util/delay.h>
#include "UserDesign.h"

AutomatStates_t State;


// ######################################################################################
// #  This function initializes the finite state machine with start state               #
// ######################################################################################
void StateMachineInit(void)
{
    State = DriveDownwards;
}

// ######################################################################################
// #  This function updates the current state of the finite state machine               #
// ######################################################################################
void StateMachineUpdate(void)
{
    switch (State)
     {
		 case DriveDownwards:
		 {
			 Actuators.DriveUpwards               = 0;
			 Actuators.DriveDownwards             = 1;
			 Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
			 Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
			 
			 if (Sensors.ElevatorOnFloor1)
			 State = DriveUpwards;
			 else if (Sensors.ElevatorAboveFloor2 & Sensors.CallButtonFloor2Up)
			 State = DriveDownx2;
			 
			 break;
		 }
         
         case DriveUpwards:
         {
	         Actuators.DriveUpwards               = 1;
	         Actuators.DriveDownwards             = 0;
			 Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
			 Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
             
             if (Sensors.ElevatorOnFloor_4)
             State = DriveDownwards;
			 else if (Sensors.ElevatorBelowFloor2 & Sensors.CallButtonFloor2Up)
			 State = DriveUpx2;
             
             break;
         }
		 
         case DriveUpx2:
         {
	         Actuators.DriveUpwards               = 1;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
	         if (Sensors.ElevatorOnFloor2 & Sensors.Floor2DoorClosed)
	         State = Open;
	         
	         break;
         }

         case DriveDownx2:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 1;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
	         if (Sensors.ElevatorOnFloor2 & Sensors.Floor2DoorClosed)
	         State = Open;
	         
	         break;
         }

         case Open:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 1;
			 Actuators.DoorFloor2Close            = 0;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	   
	         if (Sensors.Floor2DoorOpen)
	         State = Close;
			 
	         break;
         }
		 
         case Close:
         {
	         Actuators.DriveUpwards               = 0;
	         Actuators.DriveDownwards             = 0;
	         Actuators.DoorFloor2Open             = 0;
			 Actuators.DoorFloor2Close            = 1;
	         Actuators.CallDisplayFloor2Upward    = Sensors.CallButtonFloor2Up;
	         
			 if (Sensors.Floor2DoorClosed)
			 State = DriveUpwards;
			 
	         break;
         }
         
     }
}