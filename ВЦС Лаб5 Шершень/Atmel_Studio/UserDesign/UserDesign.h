// ######################################################################################
// #                                                                                    #
// #  This module implements the users design                                           #
// #                                                                                    #
// ######################################################################################

#ifndef _USERDESIGN_H
    #define _USERDESIGN_H

    #include "../Main.h"
    extern void StateMachineInit(void);                                                     // This function initializes the state machine
    extern void StateMachineUpdate(void);                                                   // This function updated the state machine

// ######################################################################################	
// #  Add a new state for state machine here                                            #
// ######################################################################################
    typedef enum
    {                                                                             
       DriveUpwards,                                                                          
       DriveDownwards, 
	   DriveUpx2, 
	   DriveDownx2, 
	   Open,
	   Close                                                                
    } AutomatStates_t;
    
#endif 