library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity num1 is 
port(
 pSW: in std_logic_vector (7 downto 0);
 pLED: out std_logic_vector(7 downto 0);
 pLED1: out std_logic_vector(7 downto 0)
 );
end num1;

architecture n1 of num1 is
begin
 pLED(6) <= transport pSW(4);
 end architecture;
