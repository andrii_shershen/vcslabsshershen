library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
entity num4 is
 
port(
	pSW: in std_logic_vector(7 downto 0);
	pLED: out std_logic_vector(7 downto 0);
	pLED1: out std_logic_vector(7 downto 0);
	clock: in std_logic);
end num4;
 
architecture n of num4 is
	signal t: integer := 0;
	signal state: std_logic := '0';
 
begin
	process is
		begin 
			wait until rising_edge(clock);
				t <= t + 1;
			if t > 5250000 then
				t <= 0;
				state <= not state;
				pLED1(0) <= state;
			end if;
	end process;
end architecture;


