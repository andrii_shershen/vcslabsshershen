library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
entity case5 is
port( 
	pSW: in std_logic_vector(7 downto 0);
	pHEX0: out std_logic_vector(7 downto 0);
	pHEX1: out std_logic_vector(7 downto 0);
	pHEX2: out std_logic_vector(7 downto 0);
	pHEX3: out std_logic_vector(7 downto 0)
);
end case5;
architecture n of case5 is
begin
	process(pSW) is
	begin 
		if (pSW(2) = '1') then
			pHEX0 <= "11000000";
			pHEX1 <= "11001000";
			pHEX2 <= "11001000";
			pHEX3 <= "10001000";
		else if (pSW(1) = '1') then
			pHEX0 <= "11111111";
			pHEX1 <= "10010010";
			pHEX2 <= "10110000";
			pHEX3 <= "10010010";
		else if (pSW(0) = '1') then
			pHEX0 <= "10110000";
			pHEX1 <= "10010010";
			pHEX2 <= "10010010";
			pHEX3 <= "10010010";
		else 
			pHEX0 <= "11000000";
			pHEX1 <= "11000000";
			pHEX2 <= "10000000";
			pHEX3 <= "10000000";
		end if; 
		end if;
		end if; 
	end process;
end n;
